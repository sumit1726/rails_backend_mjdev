Rails.application.routes.draw do
  get 'command_center', to: 'command_center#index'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  mount GraphiQL::Rails::Engine, at: "/web_api/graphql", graphql_path: "/web_api/graphql"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'application#index'
  
  get 'download/:assignment_name', to: 'downloads#download'

  get '/loaderio-bf36824434d5b257d6593cd1899d95fb/', :to => redirect('/loader.html')

  namespace :web_api do
    post 'graphql', to: 'graphql#create'
    get 'download/:assignment_name', to: 'downloads#download'
  end

  namespace :api do
    namespace :v1 do
      get 'test', to: 'test#test'
      post 'test', to: 'test#post_test'



      post 'users', to: 'users#create'
      post 'submitted_forms', to: 'submitted_forms#create'
      post 'soft_delete', to: 'submitted_forms#soft_delete'
    end
  end
end
