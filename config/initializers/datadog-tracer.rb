Rails.configuration.datadog_trace = {
  enabled: true,
  auto_instrument: false,
  auto_instrument_redis: false,
  default_service: 'rails-app',
  default_database_service: 'postgresql',
  default_cache_service: 'rails-cache',
  template_base_path: 'views/',
  tracer: Datadog.tracer,
  debug: false,
  trace_agent_hostname: 'localhost',
  trace_agent_port: 8126
}