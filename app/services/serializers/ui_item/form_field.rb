module Serializers::UiItem::FormField

  def self.run(hash)
    form_field_id = ::UiItem.find(hash[:id]).form_field_id
    if form_field_id
      include_options = (hash[:inputType] != 'string')
      x = hash[:includeAllIfNoneAssigned]
      include_all_if_none_assigned = x.present? ? (x == "true") : true
      result = {
        form_field: self.form_field_serializer(
          form_field_id,
          hash[:context_assignment_ids],
          include_options,
          include_all_if_none_assigned
        ),
        is_form_field: true
      }
    else
      result = {}
    end
    result
  end

  private

  def self.form_field_serializer(*args)
    Serializers::FormField::Main.run(*args)
  end
end
