module SubmitForm::CollectAssignmentIds

  def self.run(sf_id)
    seed_context_ids = self.seeds(sf_id)
    x = self.context_inference(seed_context_ids)
    puts "seeds : #{seed_context_ids.to_json}"
    puts "x : #{x.to_json}"
    x
  end

  private

  def self.seeds(sf_id)
    SubmitForm::SubmittedFormAssignments.run(sf_id)
  end

  def self.context_inference(*args)
    ContextInference::Main.run(*args)
  end
end
