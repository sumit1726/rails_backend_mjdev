module SubmitForm::PersistForm::CreateFormValue

  def self.run(sf_id, form_fields)
    query = form_fields.reduce("") do |acc, ff|
      if ff[:value].nil? || ff[:value] == ''
        acc
      else
        acc + self.generate_query(sf_id, ff)
      end
    end

    ActiveRecord::Base.connection.execute(query)
  end

  private

  def self.generate_query(sf_id, ff)
    case ff[:field_type]
    when 'assignment'
      self.handle_assignment(sf_id, ff)
    when 'option'
      self.handle_option(sf_id, ff)
    when 'time'
      self.handle_time(sf_id, ff)
    else
      self.handle_default(sf_id, ff)
    end
  end

  def self.handle_assignment(sf_id, ff)
    if ff[:input_type] == 'string'
      at_id = FormField.find(ff[:form_field_id]).assignment_type_id
      asg_id = Assignment.find_or_create_by(name: ff[:value], assignment_type_id: at_id).id
      fvo_id = FormValueOption.find_or_create_by(assignment_id: asg_id).id
      <<-SQL
        INSERT INTO form_values (submitted_form_id, form_field_id, form_value_option_id)
        SELECT #{sf_id}, #{ff[:form_field_id]}, #{fvo_id};
      SQL
    else
      <<-SQL
        INSERT INTO form_values (submitted_form_id, form_field_id, form_value_option_id)
        SELECT #{sf_id}, #{ff[:form_field_id]}, fvos.id
        FROM form_fields AS ffs
        JOIN assignments AS asgs
        ON asgs.assignment_type_id = ffs.assignment_type_id
        JOIN form_value_options AS fvos
        ON fvos.assignment_id = asgs.id
        WHERE asgs.id = #{ff[:value]};
      SQL
    end
  end

  def self.handle_option(sf_id, ff)
    <<-SQL
      INSERT INTO form_values (submitted_form_id, form_field_id, form_value_option_id)
      SELECT #{sf_id}, #{ff[:form_field_id]}, id
      FROM form_value_options
      WHERE id = #{ff[:value]};
    SQL
  end

  def self.handle_time(sf_id, ff)
    <<-SQL
      INSERT INTO form_values (submitted_form_id, form_field_id, datetime_value)
      VALUES (#{sf_id}, #{ff[:form_field_id]}, \'#{Time.at(ff[:value].to_f/1000).getutc.strftime('%Y-%m-%d %H:%M:%S.%N')}\');
    SQL
  end

  def self.handle_default(sf_id, ff)
    <<-SQL
      INSERT INTO form_values (submitted_form_id, form_field_id, #{ff[:field_type]}_value)
      VALUES (#{sf_id}, #{ff[:form_field_id]}, \'#{ff[:value]}\');
    SQL
  end
end
