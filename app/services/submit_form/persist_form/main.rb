module SubmitForm::PersistForm::Main

  def self.run(data)
    submitted_form = SubmittedForm.find_or_create_by(request_id: data[:request_id])
    form_fields = (data[:form_fields].length > 0) ? data[:form_fields] : data[:form_fields].concat(trivial_form_fields)

    self.create_form_value(submitted_form.id, form_fields)

    submitted_form.id
  end

  private

  def self.create_form_value(sf_id, form_fields)
    SubmitForm::PersistForm::CreateFormValue.run(sf_id, form_fields)
  end

  def self.trivial_form_fields
    assignment_type = AssignmentType.find_or_create_by(name: 'formType')
    assignment = Assignment.find_or_create_by(name: 'blankStateAppOpen', assignment_type: assignment_type)
    form_field = FormField.find_or_create_by(name: 'formType', field_type: 'assignment', assignment_type: assignment_type)
    [
      {
        form_field_id: form_field.id,
        field_type: form_field.field_type,
        input_type: 'id',
        value: assignment.id
      }
    ]
  end
end
