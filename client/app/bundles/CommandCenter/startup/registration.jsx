import ReactOnRails from 'react-on-rails';

import CommandCenter from '../components/CommandCenter';

// This is how react_on_rails can see the CommandCenter in the browser.
ReactOnRails.register({
  CommandCenter,
});
