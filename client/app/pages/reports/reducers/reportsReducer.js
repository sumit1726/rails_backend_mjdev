import { reportsState } from "../state/";
import { reportsActionTypes } from "../actions/reportsActionTypes";
import { AuthorizationRules } from "../../shared/services/authorization.rules";

export default (state = reportsState, action) => {

    let stateCopy = {};
    /**
     * Create a copy of the state on which actions will be performed.
     */
    if (reportsActionTypes[action.type]) {
        stateCopy = JSON.parse(JSON.stringify(state));
    }

    switch (action.type) {
        case reportsActionTypes.DONWLOAD_SURVEYS_EXCEL:
            {
                stateCopy.isDownloadingReport = action.payload;
                return stateCopy;
            }
        case reportsActionTypes.SET_PANEL_EXPAND_REPORTS:
            {
                stateCopy.panelProperties.panelExpanded = !stateCopy.panelProperties.panelExpanded;
                return stateCopy;
            }
        case reportsActionTypes.SET_PANEL_RELOAD_REPORTS:
            {
                stateCopy.panelProperties.panelReload = !stateCopy.panelProperties.panelReload;
                return stateCopy;
            }
        case reportsActionTypes.SET_PANEL_COLLAPSE_REPORTS:
            {
                stateCopy.panelProperties.panelCollapsed = !stateCopy.panelProperties.panelCollapsed;
                return stateCopy;
            }
        case reportsActionTypes.SET_PANEL_REMOVE_REPORTS:
            {
                stateCopy.panelProperties.panelRemoved = action.payload;
                return stateCopy;
            }
        case reportsActionTypes.SET_BOROUGHS_AND_SITES:
            {
                let data = action.payload;
                if (data != null && data.zone && data.zone.length > 0) {
                    stateCopy.filterModel.boroughs = [];
                    stateCopy.filterModel.sites = [];
                    data.zone.forEach((borough) => {
                        stateCopy.filterModel.boroughs.push({ boroughId: borough.id, boroughName: borough.name });
                        if (borough.site != null && borough.site.length > 0) {
                            borough.site.forEach(site => {
                                stateCopy.filterModel.sites.push({ boroughId: borough.id, siteId: site.id, siteName: site.name });
                            })
                        }
                    })
                    stateCopy.filterModel.sites.sort((a, b) => {
                        return (a.siteName.trim() < b.siteName.trim() ? -1 : (a.siteName.trim() > b.siteName.trim() ? 1 : 0));
                    });
                    stateCopy.filterModel.boroughs.sort((a, b) => {
                        return (a.boroughName.trim() < b.boroughName.trim() ? -1 : (a.boroughName.trim() > b.boroughName.trim() ? 1 : 0));
                    });


                    let currentUserBoroughs = AuthorizationRules.getCurrentUserBoroughNames();
                    if (currentUserBoroughs) {
                        stateCopy.filterModel.boroughs = stateCopy.filterModel.boroughs.filter((m) => currentUserBoroughs.indexOf(m.boroughName.replace(/ /g, '').toLowerCase()) > -1 || m.boroughId == -1);
                    }
                    let currentUserSites = AuthorizationRules.getCurrentUserSiteNames();
                    if (currentUserSites) {
                        stateCopy.filterModel.sites = stateCopy.filterModel.sites.filter((m) => (currentUserSites.indexOf(m.siteName.replace(/ /g, '').toLowerCase()) > -1) || m.siteId == -1);
                    }
                    // stateCopy.filterModel.selectedBorough = stateCopy.filterModel.boroughs[0];
                    return stateCopy;
                }

            }

        case reportsActionTypes.SET_SITE:
            {
                stateCopy.filterModel.selectedSite = action.payload.value;
                return stateCopy;
            }
        case reportsActionTypes.SET_BOROUGH:
            {
                stateCopy.filterModel.selectedBorough = action.payload.value;
                stateCopy.filterModel.selectedSite = null;
                // stateCopy.filterModel.sites.filter((site) => (stateCopy.filterModel.selectedBorough && site.boroughId == stateCopy.filterModel.selectedBorough.boroughId));
                // stateCopy.filterModel.selectedSite = stateCopy.filterModel.sites.filter((site) => (stateCopy.filterModel.selectedBorough && site.boroughId == stateCopy.filterModel.selectedBorough.boroughId))[0];

                return stateCopy;
            }
        default:
            return state;
    }

};