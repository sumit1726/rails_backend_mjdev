import React from "react";
import { connect } from "react-redux";
import { Constants } from "../../../../../common/app-settings/constants"
import { sharedActionTypes } from "../../../../shared/actions/sharedActionTypes";
import { CommonService } from "../../../../shared/services/common.service";
import { reportsActionTypes } from "../../../actions/reportsActionTypes";
import * as Action from "../../../../shared/actions/action";
import Select from "react-select";
import { menuRenderer } from "../../../../shared/controls/menu-renderer";
import { ReportsService } from "../../../services/reports.service";

class SurveysSubmittedComponent extends React.Component {

    constructor(props) {
        super(props);
        this.onExcelDownload = this.onExcelDownload.bind(this);
        this.siteFilterOptions = this.siteFilterOptions.bind(this);
        this.boroughsFilterOptions = this.boroughsFilterOptions.bind(this);
        this.onBoroughChange = this.onBoroughChange.bind(this);
        this.onSiteChange = this.onSiteChange.bind(this);
        this.fetchBoroughsAndSites = this.fetchBoroughsAndSites.bind(this);
    }
    /**
    * Set tab to DownloadSurveys on component mount.
    */
    componentDidMount() {
        this.fetchBoroughsAndSites();
        this.props.dispatch(Action.getAction(sharedActionTypes.SET_TAB_CHANGE, { key: Constants.reportsViewKeys.surveysSubmitted }));
    }
    // download excel file containing all surveys submitted yet for all sites
    onExcelDownload() {
        
        console.log("onExcelDownload",this.props.model.filterModel.selectedSite);
        CommonService.downloadExcel("assignmentName");
    }
    /**
     * Filter sites for selected borough.
     */
    siteFilterOptions(sites, selectedBorough) {
        if (selectedBorough) {
            this.sitesOptions = sites && sites.filter((site) => (site.boroughId == selectedBorough.boroughId));
            // if there is only one site make that selected in drop down
            let allSites = this.sitesOptions && this.sitesOptions.filter(m => m.siteId != -1);
            if (allSites && allSites.length == 1)
                this.sitesOptions = allSites;
        }
    }

    /**
     * Sort boroughs.
     */
    boroughsFilterOptions(boroughs) {
        this.boroughsOptions = boroughs && boroughs.sort(function (a, b) {
            let value = 0;
            a.boroughName < b.boroughName ? (value = -1) : (a.boroughName > b.boroughName ? (value = 1) : (value = 0));
            return value;
        })

        // if there is only one borough set that selected 
        let allboroughs = boroughs && boroughs.filter(m => m.boroughId != -1);
        if (allboroughs && allboroughs.length == 1) {
            boroughs = allboroughs;
        }
    }

    onBoroughChange(value){
        this.props.dispatch(Action.getAction(reportsActionTypes.SET_BOROUGH, { value: value }));        
    }

    onSiteChange(value) {
        this.props.dispatch(Action.getAction(reportsActionTypes.SET_SITE, { value: value }));
    }

    fetchBoroughsAndSites() {
        ReportsService.getBoroughsAndSites(this.props.sharedModel.selectedQCInstances).then(
            (response) => {
                this.props.dispatch(Action.getAction(reportsActionTypes.SET_BOROUGHS_AND_SITES, response));
                // if there is site selcted then fetch all teams of this site
                if (this.props.model.filterModel.selectedSite)
                    this.onSiteChange(this.props.model.filterModel.selectedSite);
            }
        ).catch((error) => {
            console.log(error);
        });
    }


    render() {
        let reportsModel = this.props.model;
        return (
            <div>
                <div className="admin-filter-bar">
                    <table cellSpacing="0" cellPadding="0" >
                        <tbody>
                            <tr>
                                <td className="filter-boroughs">
                                    <label>
                                        Borough<span className="asterik-white">*</span>
                                    </label>
                                    <Select value={reportsModel.filterModel.selectedBorough} valueKey="boroughId" labelKey="boroughName" searchable={false} clearable={false}
                                        menuRenderer={menuRenderer} filterOptions={this.boroughsFilterOptions(reportsModel.filterModel.boroughs)}
                                        name="form-field-name" onChange={this.onBoroughChange} options={this.boroughsOptions} />
                                </td>

                                <td className="filter-sites">
                                    <label>
                                        Site<span className="asterik-white">*</span>
                                    </label>
                                    <Select value={reportsModel.filterModel.selectedSite} valueKey="siteId" labelKey="siteName" searchable={false} clearable={false}
                                        menuRenderer={menuRenderer} filterOptions={this.siteFilterOptions(reportsModel.filterModel.sites, reportsModel.filterModel.selectedBorough)} name="form-field-name" onChange={this.onSiteChange}
                                        options={this.sitesOptions}  />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <table className="routeList-table routeList-table-rounded">
                    <thead>
                        <tr>
                            <th>Route</th>
                            <th>Team</th>
                            <th>Status</th>
                            <th>Borough</th>
                            <th>Site</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        {/*{  
                    searchedRoutes.map((route, index)=> {

                        return (
                            <tr key={"route-list-view-"+index}>
                                <td>{Utility.getSubwayRouteName(route)} <img src={route_icon} alt="" title="View Route Map" className="route-icon" onClick={()=> {this.onOpenRouteOnMapDialog(route)}} /></td>
                                <td><label className={route.teamLabel ? '' : 'members-route no-members'}>{route.teamLabel ? (route.teamLabel) : 'Unassigned Team'}</label> <img src={teams_icon} alt="" className="route-icon displaynone" /></td>
                                <td className={route.routeStatus == Constants.routeStatusKey.notStarted.key ? 'routeList-table routeList-table-rounded reactable-data teamStatusNotStarted' : (route.routeStatus == Constants.routeStatusKey.completed.key ? 'routeList-table routeList-table-rounded reactable-data teamStatusComplete' : (route.routeStatus == Constants.routeStatusKey.inProgress.key ? 'routeList-table routeList-table-rounded reactable-data teamStatusInProgress' : 'routeList-table routeList-table-rounded reactable-data'))} >{route.routeStatus == Constants.routeStatusKey.notStarted.key ? Constants.routeStatusKey.notStarted.value : (route.routeStatus == Constants.routeStatusKey.completed.key ? Constants.routeStatusKey.completed.value : (route.routeStatus == Constants.routeStatusKey.inProgress.key ? Constants.routeStatusKey.inProgress.value : ''))}</td>
                                <td>{route.boroughName} </td>
                                <td>{route.siteName} </td>
                                <td>{route.routeType}</td>
                            </tr>
                        );
                    })  }  */}
                    </tbody>
                </table>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        model: state.reportsModel,
        sharedModel: state.sharedModel
    };
}
export default connect(mapStateToProps)(SurveysSubmittedComponent);;