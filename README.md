# GraphQL API for Web Front-End


## Example 1:

### Input

(suppose 3423 is the id of a countInstance)

```
queryString = "
  {
    assignmentGraph(
      contextAssignmentIds: [3423],
      types: [
        {name: \"route\", properties: [\"status\", "needs_nypd"]},
        {name: \"site\", properties: []}
      ]
    ) {
      assignments {
        id
        name
        label
        assignmentType {
          id
          name
        }
        properties
      }
      assignmentRelations {
        id
        assignment1Id
        assignment2Id
      }
    }
  }
"

NOTE : the above string was written on multiple lines for readability, but javascript
will not let you write strings this way (strings must be written on a single line).
To avoid having to write the entire string on a single line, you can use Babel (https://babeljs.io/) to enable ES6 template literals (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals).

For the sake of testing before ES6 is enabled, here is a copy of the same queryString from above except expressed
on a single line :

queryString = "{ assignmentGraph( contextAssignmentIds: [13086], types: [ {name: \"route\", properties: [\"status\", \"needs_nypd\"]}, {name: \"site\", properties: []}]) {assignments {id name label assignmentType { id name } properties } assignmentRelations { id assignment1Id assignment2Id } } }"

$.ajax({
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify({query: queryString})
    success: function (response) {
      ...
    }
})

```

### Output
```
  {
    assignmentGraph: {
      assignments: [
        {
          id: 1,
          name: 'Hostos:101',
          label: '101',
          assignmentType: {id: 1, name: 'route'},
          properties: {status: 'not_started', needs_nypd: 'true'}          
        },
        {
          id: 2,
          name: 'Hostos:102',
          label: '102',
          assignmentType: {id: 1, name: 'route'},
          properties: {status: 'in_progress', needs_nypd: 'false'}          
        },
        {
          id: 3,
          name: 'Hostos',
          label: 'Hostos',
          assignmentType: {id: 1, name: 'site'},
          properties: {}          
        }
      ],
      assignmentRelations: [
        {
          id: 1,
          assignment1Id: 1,
          assignment2Id: 3
        },
        {
          id: 2,
          assignment1Id: 2,
          assignment2Id: 3        
        }
      ]
    }
  }

```


## Example 2:

### Sample working query:
https://qcdev.dhsportal.nyc:444/graphql?query=%7B%0A%20%20%20%20assignmentGraph(%0A%20%20%20%20%20%20contextAssignmentIds%3A%20%5B13086%5D%2C%0A%20%20%20%20%20%20types%3A%20%5B%0A%20%20%20%20%20%20%20%20%7Bname%3A%20%22route%22%2C%20properties%3A%20%5B%22status%22%2C%20%22needs_nypd%22%5D%7D%2C%0A%20%20%20%20%20%20%20%20%7Bname%3A%20%22site%22%2C%20properties%3A%20%5B%5D%7D%0A%20%20%20%20%20%20%5D%0A%20%20%20%20)%20%7B%0A%20%20%20%20%20%20assignments%20%7B%0A%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20label%0A%20%20%20%20%20%20%20%20assignmentType%20%7B%0A%20%20%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20%20%20name%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20properties%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20assignmentRelations%20%7B%0A%20%20%20%20%20%20%20%20id%0A%20%20%20%20%20%20%20%20assignment1Id%0A%20%20%20%20%20%20%20%20assignment2Id%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%0A%7D














/
